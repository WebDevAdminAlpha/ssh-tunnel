FROM alpine:latest

RUN apk add --update openssh-client

ENTRYPOINT ["ssh"]
