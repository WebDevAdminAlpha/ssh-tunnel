# SSH Tunnel

Simply, this is an image that provides internet connection to docker containers from CI.

See [`gitlab-org/gitlab-qa/blob/master/lib%2Fgitlab%2Fqa%2Fcomponent%2Finternet_tunnel.rb`](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/lib%2Fgitlab%2Fqa%2Fcomponent%2Finternet_tunnel.rb#L6)

# Release Process

Currently this image is released manually to [Docker Hub](https://hub.docker.com/)

```
$ docker build . -t gitlab/ssh-tunnel:<version>
$ docker push gitlab/ssh-tunnel:<version>
```

An [issue is created](https://gitlab.com/gitlab-org/quality/ssh-tunnel/issues/1) for hooking up to CI/CD.
